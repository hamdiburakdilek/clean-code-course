package io.ozkan.cleancode.fizzbuzz;

/**
 * @author Omer Ozkan
 */
public class FizzBuzz {

    public static String classify(int number) {
        String result = "";

        if(number % 3 == 0) {
            result += "Fizz";
        }
        if(number % 5 == 0) {
            result += "Buzz";
        }
        return result.isEmpty() ? number + "" : result;
    }
}

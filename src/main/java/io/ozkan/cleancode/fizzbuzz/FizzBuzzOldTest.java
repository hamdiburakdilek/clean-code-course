package io.ozkan.cleancode.fizzbuzz;

/**
 * @author Omer Ozkan
 */
public class FizzBuzzOldTest {

    public static void main(String[] args) {
        assertEquals("1", FizzBuzz.classify(1));
        assertEquals("Fizz", FizzBuzz.classify(3));
        assertEquals("Buzz", FizzBuzz.classify(5));
        assertEquals("FizzBuzz", FizzBuzz.classify(15));
    }

    private static void assertEquals(Object expected, Object actual) {
        if(!actual.equals(expected)) {
            throw new AssertionError("Expected "+ expected +" but actual " + actual);
        }
    }
}

package io.ozkan.cleancode.ocp;

/**
 * @author Omer Ozkan
 */
public class RemoteControllerTest {
    public static void main(String[] args) {
        TV tv = new TV();
        TV2 tv2 = new TV2();
        BluRayPlayer bluRayPlayer = new BluRayPlayer();
        SoundSystem soundSystem = new SoundSystem();

        RemoteController rc = new RemoteController();

        rc.on(tv);
        rc.on(bluRayPlayer);
        rc.on(soundSystem);

        rc.off(tv);
        rc.off(bluRayPlayer);
        rc.off(soundSystem);

        rc.on(tv2);
        rc.off(tv2);

        RemoteController2 rc2 = new RemoteController2();
        rc2.volumeDown(tv2);
        rc2.volumeUp(tv2);
        rc2.on(tv);
        rc2.off(tv);
    }
}

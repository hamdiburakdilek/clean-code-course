package io.ozkan.cleancode.ocp;

/**
 * @author Omer Ozkan
 */
public class BluRayPlayer implements Device {

    @Override
    public void on() {
        System.out.println("BluRay turned on");
    }

    @Override
    public void off() {
        System.out.println("BluRay turned off");
    }
}

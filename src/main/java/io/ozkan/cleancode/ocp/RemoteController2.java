package io.ozkan.cleancode.ocp;

/**
 * @author Omer Ozkan
 */
public class RemoteController2 extends RemoteController {

    public void volumeUp(Device2 device) {
        device.vUp();
    }

    public void volumeDown(Device2 device) {
        device.vDown();
    }
}

package io.ozkan.cleancode.ocp;

/**
 * @author Omer Ozkan
 */
public class TV implements Device {

    @Override
    public void on() {
        System.out.println("TV Turned On");
    }

    @Override
    public void off() {
        System.out.println("TV Turned Off");
    }
}

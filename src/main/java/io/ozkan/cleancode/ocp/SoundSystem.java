package io.ozkan.cleancode.ocp;

/**
 * @author Omer Ozkan
 */
public class SoundSystem implements Device {
    @Override
    public void on() {
        System.out.println("SoundSystem turned on");
    }

    @Override
    public void off() {
        System.out.println("SoundSystem turned on");
    }
}

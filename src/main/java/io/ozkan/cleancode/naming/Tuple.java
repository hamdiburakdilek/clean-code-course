package io.ozkan.cleancode.naming;

/**
 * This is bad. Reaaly bad. It's a really, really bad hack.
 * If you're employee of Intertrode Communication,
 * them I am really, really sorry that you have to maintain
 * this I was honestly planning on removing this tomorrow,
 * but I've been known to forget things like this. It happens.
 * <p>
 * So here's the thing.
 * I can't seem to figure out why the AccountId variable isn't set.
 * I've looked and looked, but I gotto leave now.
 * Anyway, I've found that I can just grap the AccountId from debuging logs.
 * I suppose that to fix it you'd have to locate where it's clearing out the ID.
 * <p>
 * Again, I'm sorry.
 *
 * TODO remove this class
 *
 * Added by cukubik
 */
public class Tuple<V1, V2> {

    //////////
    // Instance Variables
    ///////////////////////
    private final V1 v1;
    private final V2 v2;

    //////////
    // Constructors
    //////////////////

    public Tuple(V1 v1,
                 V2 v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    ///
    // METHODS
    /////////////////////////////////////////

    public V1 v1() {
        return v1;
    }

    public V2 v2() {
        return v2;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tuple tuple = (Tuple) o;

        if (v1 != null ? !v1.equals(tuple.v1) : tuple.v1 != null) return false;
        if (v2 != null ? !v2.equals(tuple.v2) : tuple.v2 != null) return false;

        return true;
    }

    public int hashCode() {
        int result = v1 != null ? v1.hashCode() : 0;
        result = 31 * result + (v2 != null ? v2.hashCode() : 0);
        return result;
    }

    public static <V1, V2> Tuple<V1, V2> create(V1 v1, V2 v2) {
        return new Tuple<>(v1, v2);
    }
}

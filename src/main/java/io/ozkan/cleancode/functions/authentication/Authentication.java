package io.ozkan.cleancode.functions.authentication;

import java.util.Map;

/**
 * @author Omer Ozkan
 * @version 26/01/16
 */
public class Authentication {
    private Map<String, String> parameters;
    private String principle;
    private String credentials;

    public Authentication(String principle, String credentials, Map<String, String> parameters) {
        this.principle = principle;
        this.credentials = credentials;
        this.parameters = parameters;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getPrinciple() {
        return principle;
    }

    public String getCredentials() {
        return credentials;
    }
}

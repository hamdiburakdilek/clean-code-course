package io.ozkan.cleancode.functions.authentication;

/**
 * @author Omer Ozkan
 * @version 26/01/16
 */
public class AuthenticationException extends Exception {
    public AuthenticationException(String msg) {
        super(msg);
    }
}

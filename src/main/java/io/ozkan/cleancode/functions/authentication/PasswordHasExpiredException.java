package io.ozkan.cleancode.functions.authentication;

/**
 * @author Omer Ozkan
 * @version 26/01/16
 */
public class PasswordHasExpiredException extends AuthenticationException {
    public PasswordHasExpiredException(String msg) {
        super(msg);
    }
}

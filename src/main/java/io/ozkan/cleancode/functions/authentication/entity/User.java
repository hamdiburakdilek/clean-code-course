package io.ozkan.cleancode.functions.authentication.entity;

/**
 * @author Omer Ozkan
 * @version 26/01/16
 */
public class User {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

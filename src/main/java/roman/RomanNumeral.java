package roman;

import org.omg.SendingContext.RunTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RomanNumeral {

    private static final Map<String,Integer> DIGITS = new HashMap<>();
    static {
        DIGITS.put("I",1);
        DIGITS.put("V",5);
        DIGITS.put("X",10);
        DIGITS.put("L",50);
        DIGITS.put("C",100);
        DIGITS.put("D",500);
        DIGITS.put("M",1000);
    }

    public int convert(String input) {
        int result = 0;
        int lastInteger = Integer.MAX_VALUE;
        String[] inputStringArray = input.split("");
        for(String digit : inputStringArray){

            if(!DIGITS.containsKey(digit)){
                throw new UnknownDigit(digit);
            }

            if (lastInteger< DIGITS.get(digit)){
                result -= lastInteger*2;
            }

            lastInteger = DIGITS.get(digit);

            result += lastInteger;
        }


        return result;
    }
    static class UnknownDigit extends RuntimeException {
        public UnknownDigit(String digit) {
            super(digit + "is unkonwn");
        }
    }
}

package io.ozkan.cleancode.fizzbuzz;

import org.junit.Test;
import static org.junit.Assert.*;

public class FizzBuzzTest {

    @Test
    public void classifyDivisibleBy3AsFizz() {
        assertEquals("Fizz", FizzBuzz.classify(3));
        assertEquals("Fizz", FizzBuzz.classify(6));
        assertEquals("Fizz", FizzBuzz.classify(9));
    }

    @Test
    public void classify5AsBuzz() {
        assertEquals("Buzz", FizzBuzz.classify(5));
        assertEquals("Buzz", FizzBuzz.classify(10));
        assertEquals("Buzz", FizzBuzz.classify(20));
    }

    @Test
    public void classify15AsFizzBuzz() {
        assertEquals("FizzBuzz", FizzBuzz.classify(15));
        assertEquals("FizzBuzz", FizzBuzz.classify(30));
        assertEquals("FizzBuzz", FizzBuzz.classify(45));
    }

    @Test
    public void classifyDivisibleByNeither3Nor5AsIs() {
        assertEquals("1", FizzBuzz.classify(1));
        assertEquals("7", FizzBuzz.classify(7));
        assertEquals("11", FizzBuzz.classify(11));
    }


}

package roman;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RomanNumeralTest {


    private final RomanNumeral roman = new RomanNumeral();

    @Test
    public void convertIToOne() throws Exception {
        assertEquals(1, roman.convert("I"));
    }
    @Test
    public void convertIIToTwo() throws Exception {
        assertEquals(2, roman.convert("II"));
    }
    @Test
    public void convertIIIToThree() throws Exception {
        assertEquals(3, roman.convert("III"));
    }

    @Test
    public void convertIVToFour() throws Exception {
        assertEquals(4, roman.convert("IV"));
    }

    @Test
    public void convertVTOFive() throws Exception {
        assertEquals(5,roman.convert("V"));
    }

    @Test
    public void convertVIITOEight() throws Exception {
        assertEquals(8,roman.convert("VIII"));
    }

    @Test
    public void convertXToTen() throws Exception {
        assertEquals(10,roman.convert("X"));
    }
    @Test
    public void convertLToFifty() throws Exception {
        assertEquals(50,roman.convert("L"));
    }

    @Test
    public void convertCToHundered() throws Exception {
        assertEquals(100,roman.convert("C"));
    }
    @Test
    public void convertCXXVIIToHunderedAndTwentyTwo() throws Exception {
        assertEquals(127,roman.convert("CXXVII"));
    }
    @Test
    public void convertLToFiveHundered() throws Exception {
        assertEquals(500,roman.convert("D"));
    }

    @Test
    public void convertMCCCLVIIIToOneThousandThreeHundredAndFiftyEight() throws Exception {
        assertEquals(1358,roman.convert("MCCCLVIII"));
    }

    @Test
    public void convertMToThousand() throws Exception {
        assertEquals(1000,roman.convert("M"));
    }

    @Test(expected = RomanNumeral.UnknownDigit.class)
    public void throwExceptionWhenZIsGiven() throws Exception {
        roman.convert("Z");
    }
}
